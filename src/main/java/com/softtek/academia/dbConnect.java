package com.softtek.academia;

import java.sql.Connection;
import java.sql.DriverManager;

public class dbConnect {        
    static Connection con=null;
    public static Connection getConnection()
    {
        if (con != null) return con;
        // get db, user, pass from settings file
        //return getConnection("jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#", "root", "1234");
        return getConnection("sesion3?serverTimezone=UTC#", "root", "1234");
    }

    private static Connection getConnection(String db_name,String user_name,String password)
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            con=DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#", "root", "1234");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        return con;        
    }
} 
