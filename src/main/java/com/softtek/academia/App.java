package com.softtek.academia;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App 
{
    public static void main( String[] args ) throws SQLException
    {
    	List<Device> list = new ArrayList<>();
        String SQL_SELECT = "Select * from DEVICE";
        Connection conn = dbConnect.getConnection();

            if (conn != null) {
                PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                	Device device = new Device(resultSet.getLong("DEVICEID"),resultSet.getString("NAME"),resultSet.getString("DESCRIPTION"),resultSet.getLong("MANUFACTURERID"),resultSet.getLong("COLORID"),resultSet.getString("COMMENTS"));
                    list.add(device);
                }
            } else {
                System.out.println("Failed to make connection!");
            }
            System.out.println("LISTA DE TODOS LOS DISPOSITIVOS:\n");
            for (Device c:list){
            	System.out.println("ID: " + c.getDeviceId() + "\nNombre: " + c.getName() + "\nDescripcion: " + c.getDescription() + "\nFabricante: " + c.getManufacturerId() + "\nColor: " + c.getColorId() + "\nComentarios: " + c.getComments() + "\n");
            }
            System.out.println("____________________________\n");
            List<String> laptops = list.stream()
    		.map(Device::getName)
    		.filter(device->device.equals("Laptop"))
    		//.reduce("", (string, valor) -> string + valor + "\n");
    		.collect(Collectors.toList());
            System.out.println("Lista de laptops: \n");
            laptops.forEach(System.out::println);
            System.out.println("____________________________\n");
            
            
            long apple=list.stream().map(Device::getManufacturerId).filter(device-> device.toString().equals("3")).count();
            System.out.println("Numero de dispositivos apple: "+apple);
            System.out.println("____________________________\n");
            
            List<Device> blancos = list.stream().filter(device -> (device.getColorId()==3)).collect(Collectors.toList());
            System.out.println("Devices blancos: ");
            for (Device c:blancos){
            	System.out.println("\nID: " + c.getDeviceId() + "\nNombre: " + c.getName() + "\nDescripcion: " + c.getDescription() + "\nFabricante: " + c.getManufacturerId() + "\nColor: " + c.getColorId() + "\nComentarios: " + c.getComments());
            }
            System.out.println("____________________________\n");
            
            Map<Object, Object> mapa = list.stream().collect(Collectors.toMap(
    	            Device::getDeviceId,
    	            device -> device
    	        ));
    		
            System.out.println("Mapa de Devices:\n");
            System.out.println("   ID	   Objeto");
    		mapa.forEach((key, value) -> System.out.println("   "+key + "	|  " + value));
    		System.out.println("____________________________\n");
            	
            };
            
            

 }
